'use strict'

const inputData = require('./input.json')
const Hapi = require('@hapi/hapi')
const Joi = require('joi')
const { mapInputLevel } = require('./mapInputLevel')
const { githubSearch } = require('./githubSearch')

const home = (request, h) => {
  // Prevent data injection to json file.
  // const cloneData = JSON.parse(JSON.stringify(inputData))
  const cloneData = request?.payload
  const data = mapInputLevel(cloneData)
  return data
}

const init = async () => {
  const server = Hapi.server({
    port: 3000,
    host: 'localhost',
    routes: {
      cors: true,
    },
  })

  await server.register([
    require('inert'),
    require('vision'),
    {
      plugin: require('hapi-swaggered'),
      options: {
        info: {
          title: 'Pomelo rest-api document',
          version: '1.0',
        },
      },
    },
    {
      plugin: require('hapi-swaggered-ui'),
      options: {
        title: 'Example API',
        path: '/docs',
        authorization: {
          field: 'apiKey',
          scope: 'query', // header works as well
          // valuePrefix: 'bearer '// prefix incase
          defaultValue: 'demoKey',
          placeholder: 'Enter your apiKey here',
        },
        swaggerOptions: {
          validatorUrl: null,
        },
      },
    },
  ])

  server.route([
    {
      method: 'POST',
      path: '/format',
      options: {
        tags: ['api'],
        description: 'Route for format input',
        notes:
          'Example data in folder server/input.json, You can copy all context and paste here.',
        validate: {
          payload: Joi.object(),
        },
        handler: home,
      },
    },
    {
      method: 'GET',
      path: '/github-search',
      options: {
        tags: ['api'],
        description: 'Route for get github repositories',
        validate: {
          query: Joi.object({
            page: Joi.string().default(1).required(),
          }),
        },
        handler: githubSearch,
      },
    },
  ])

  await server.start()
  console.log('Server running on %s', server.info.uri)
}

process.on('unhandledRejection', (err) => {
  console.log(err)
  process.exit(1)
})

init()
