const mapInputLevel = function (data) {
  if (!data || typeof data !== 'object') return []
  let newValue = []
  let output = []
  // Group data to one array
  for (let index = 0; index < Object.keys(data).length; index++) {
    const element = data[index]
    // Create new array
    element.map((v) => newValue.push(v))
    // Process logic in last of lists
    if (index === Object.keys(data).length - 1) {
      // Sort level from max to min
      newValue.sort((a, b) => b.level - a.level)
      // Map value to level, Filter value only has parent_id and level more than 0
      newValue
        .filter((v) => v.parent_id && v.level > 0)
        .map((v) => {
          // Number minus >= 0 is has previous level
          const hasPreviousLevel = v.level - 1 >= 0
          if (hasPreviousLevel) {
            // Get parent id of previous level
            const previousIndex = newValue.findIndex(
              (i) => i.id === v.parent_id
            )
            // If match then push data to previous level
            if (previousIndex > -1) {
              newValue[previousIndex].children.push(v)
            }
          }
        })
      // Clear garbage data in first level
      output = newValue.filter((v) => !v.parent_id && v.level === 0)
    }
  }
  return output
}

module.exports = {
  mapInputLevel,
}
