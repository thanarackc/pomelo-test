const expect = require('chai').expect
const { githubSearch } = require('../githubSearch')

describe('Github Search Api', function () {
  let data

  before(async function () {
    data = await githubSearch({ query: { page: 1 } })
  })

  describe('#githubSearch.test()', function () {
    it('should return type object', function () {
      expect(data).to.an('object')
    })
    it('should return correct format', async function () {
      expect(data).to.have.all.keys(
        'total_count',
        'items',
        'incomplete_results'
      )
    })
  })
})
