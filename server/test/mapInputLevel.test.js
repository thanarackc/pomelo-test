const expect = require('chai').expect;
const { mapInputLevel } = require('../mapInputLevel')
const input = require('../input.json')

describe('json format function', function() {
  describe('#mapInputLevel()', function() {
    it('should return type array', function() {
      const data = mapInputLevel(input)
      expect(data).to.be.an('array')
    });
    it('should map correct', function() {
      const data = mapInputLevel(input)
      expect(data).to.have.length(1)
    });
    it('should number 10 at frist array', function() {
      const data = mapInputLevel(input)
      expect(data[0].id).to.equal(10)
    });
    it('should title Red Roof at level 1', function() {
      const data = mapInputLevel(input)
      expect(data[0].children[0].title).to.equal('Red Roof')
    });
    it('should title Red Window at level 2 and second object', function() {
      const data = mapInputLevel(input)
      expect(data[0].children[0].children[1].title).to.equal('Red Window')
    });
  });
});