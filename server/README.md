# Server side

## Project setup
```
yarn install
```

### Compiles and hot-reloads for development
```
yarn dev
```

### test
```
yarn test
```


### Enter to api
```
localhost:3000
```

### swager url
```
localhost:3000/docs
```
