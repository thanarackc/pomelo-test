const axios = require('axios')

const githubSearch = async function (request, h) {
  const { page } = request.query
  let data = []
  await axios
    .get(
      `https://api.github.com/search/repositories?q=nodejs&page=${
        +page || 1
      }&per_page=10`
    )
    .then((res) => {
      data = res.data
    })
    .catch((err) => {
      console.log('Error: ', err.message)
    })

  return data
}

module.exports = {
  githubSearch,
}
