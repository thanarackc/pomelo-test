# Pomelo test by thanarak chaisri

## Project start server
```
yarn start-server
```

## Project start client
```
yarn start-client
```

### Part 1 assignments & Method post
```
Also please check swagger here : localhost:3000/docs#/format/post_format
endpoint: localhost:3000/format
```

### Part 2 assignments
```
server: localhost:3000
service: localhost:3000/github-search?page=1
client: localhost:8000
```