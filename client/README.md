# Client

## Project setup
```
yarn install
```

### Compiles and hot-reloads for development
```
yarn serve
```

### Compiles and minifies for production
```
yarn build
```

### test and fixes files
```
yarn lint
```

### Enter to website
```
localhost:8000
```


### Customize configuration
See [Configuration Reference](https://cli.vuejs.org/config/).
